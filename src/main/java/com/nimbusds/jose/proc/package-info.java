/**
 * Secure framework for application-specific processing of JOSE objects (with
 * arbitrary payloads). To process JSON Web Tokens (JWT) refer to the
 * {@link com.nimbusds.jwt.proc} package.
 */
package com.nimbusds.jose.proc;